package com.thebestrating;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThebestratingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThebestratingApplication.class, args);
	}

}
